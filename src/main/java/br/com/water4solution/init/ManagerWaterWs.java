package br.com.water4solution.init;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import br.com.water4solution.model.Arduino;
import br.com.water4solution.model.RecordWater;
import br.com.water4solution.service.ArduinoService;
import br.com.water4solution.service.WaterRecordService;

@Path("/arduino")
public class ManagerWaterWs {
	private ArduinoService ardService;
	private WaterRecordService wrService;
	private Logger logger;
	
	public ManagerWaterWs() {
		ardService = new ArduinoService();
		wrService = new WaterRecordService();
		logger = Logger.getLogger(ManagerWaterWs.class);
	}
	
	@Path("/sendData")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response update(
			@FormParam("ardId") String ardId,
			@FormParam("input") double input,
			@FormParam("output") double output) {

		logger.debug("Arduino - SendData - START");
		Arduino arduino = new Arduino();
		arduino.setMacAdress(ardId);
		
		arduino = ardService.select(arduino);
		
		if (arduino == null) {
			return Response.ok("Invalid Parameters").build();
		}
		
		RecordWater recordWater = new RecordWater();
		recordWater.setArduino(arduino);
		recordWater.setReadEntry(input);
		recordWater.setReadOut(output);
		recordWater.setDate(new Date());
		
		wrService.insert(recordWater);
		logger.debug("Arduino - SendData - END");
		return Response.ok("OK").build();
	}
	
	@Path("/list") @POST @Produces({MediaType.APPLICATION_JSON})
	public Response list(@FormParam("ardId") int ardId){
		logger.debug("select - START");
		Arduino ard = new Arduino();
		ard.setId(ardId);
		List<RecordWater> listRecords = new ArrayList<RecordWater>();
		listRecords = wrService.select(ard);
		logger.debug("select - END");
		return Response.ok(listRecords).build();
	}
	
	@Path("/get") @POST @Produces({MediaType.APPLICATION_JSON})
	public Response getRecord(
			@FormParam("recordId") int recordId){
		logger.debug("getRecord - START");

		RecordWater recordWater = new RecordWater();
		recordWater.setRecordId(recordId);
		recordWater = wrService.getRecordById(recordWater);
		
		if (recordWater == null) {
			return Response.ok("Invalid Parameters").build();
		}
		
		logger.debug("getRecord - END");
		return Response.ok(recordWater).build();
			}
}

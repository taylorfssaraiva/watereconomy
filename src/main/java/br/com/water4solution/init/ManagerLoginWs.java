package br.com.water4solution.init;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;


public class ManagerLoginWs {
	@Path("/login")
	public Response login(){
		return Response.ok().build();
	}
}

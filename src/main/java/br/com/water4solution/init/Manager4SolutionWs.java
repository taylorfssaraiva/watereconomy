package br.com.water4solution.init;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
public class Manager4SolutionWs {

	@Produces({MediaType.APPLICATION_JSON})
	public static boolean validadeToken(String token){
		if (token == null) {
			Response.serverError();
		}
		return true;
	}
}

package br.com.water4solution.service;

import java.util.List;

import org.apache.log4j.Logger;

import br.com.water4solution.dao.WaterRecordDAO;
import br.com.water4solution.model.Arduino;
import br.com.water4solution.model.RecordWater;

public class WaterRecordService {
	private WaterRecordDAO dao;
	private Logger log;

	public WaterRecordService() {
		dao = new WaterRecordDAO();
		log = Logger.getLogger(WaterRecordService.class);
	}
	
	public void insert(RecordWater recWater){
		log.debug("Insert - START");
		dao.insert(recWater);
		log.debug("Insert - END");
	}
	
	public void update(RecordWater recWater) {
		log.debug("update - Start");
		dao.update(recWater);
		log.debug("update - End");
	}

	public List<RecordWater> select(Arduino ardid) {
		log.debug("Select - Start");
		List<RecordWater> ardTemp = null;
		ardTemp = dao.select(ardid);
		log.debug("Select - End");
		return ardTemp;
	}

	public RecordWater getRecordById(RecordWater recordWater) {
		log.debug("Select - Start");
		RecordWater recordTemp = null;
		recordTemp = dao.getRecordById(recordWater);
		log.debug("Select - End");

		return recordTemp;
	}

}

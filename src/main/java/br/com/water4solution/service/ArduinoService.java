package br.com.water4solution.service;

import org.apache.log4j.Logger;

import br.com.water4solution.dao.ArduinoDAO;
import br.com.water4solution.model.Arduino;

public class ArduinoService {
	private ArduinoDAO dao;
	private Logger log;
	
	public ArduinoService() {
		dao = new ArduinoDAO();
		log = Logger.getLogger(ArduinoService.class);
	}

	public void update(Arduino arduino) {
		log.debug("update - Start");
		dao.update(arduino);
		log.debug("update - End");
	}

	public Arduino select(Arduino arduino) {
		log.debug("Select - Start");
		Arduino ardTemp = null;
		ardTemp = dao.select(arduino);
		log.debug("Select - End");
		return ardTemp;
	}
	
	

}

package br.com.water4solution.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;
import javax.ws.rs.DefaultValue;

import com.sun.istack.NotNull;

@Entity @Inheritance(strategy = InheritanceType.JOINED)
public class Client {
	@Id @GeneratedValue @NotNull
	private int id;
	@NotNull
	private String name;
	@OneToOne @NotNull
	private ClientAdress adress;
	@OneToOne @NotNull
	private Arduino arduino;
	@NotNull @DefaultValue(value = "1000")
	private int tankSize;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the adress
	 */
	public ClientAdress getAdress() {
		return adress;
	}
	/**
	 * @param adress the adress to set
	 */
	public void setAdress(ClientAdress adress) {
		this.adress = adress;
	}
	/**
	 * @return the arduino
	 */
	public Arduino getArduino() {
		return arduino;
	}
	/**
	 * @param arduino the arduino to set
	 */
	public void setArduino(Arduino arduino) {
		this.arduino = arduino;
	}
	/**
	 * @return the tankSize
	 */
	public int getTankSize() {
		return tankSize;
	}
	/**
	 * @param tankSize the tankSize to set
	 */
	public void setTankSize(int tankSize) {
		this.tankSize = tankSize;
	}
	
	
	
}

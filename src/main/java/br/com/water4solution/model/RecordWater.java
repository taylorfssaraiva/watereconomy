package br.com.water4solution.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.ws.rs.DefaultValue;

import com.sun.istack.NotNull;

@Entity
public class RecordWater implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id @GeneratedValue
	private int recordId;
	@ManyToOne(cascade = CascadeType.ALL) @NotNull
	private Arduino arduino;
	@NotNull @DefaultValue(value="0")
	private double readEntry;
	@NotNull @DefaultValue(value="0")
	private double readOut;
	@NotNull
	private Date date;
	
	/**
	 * @return the id
	 */
	public int getRecordId() {
		return recordId;
	}
	/**
	 * @param id the id to set
	 */
	public void setRecordId(int id) {
		this.recordId = id;
	}
	/**
	 * @return the arduino
	 */
	public Arduino getArduino() {
		return arduino;
	}
	/**
	 * @param arduino the arduino to set
	 */
	public void setArduino(Arduino arduino) {
		this.arduino = arduino;
	}
	/**
	 * @return the readEntry
	 */
	public double getReadEntry() {
		return readEntry;
	}
	/**
	 * @param readEntry the readEntry to set
	 */
	public void setReadEntry(double readEntry) {
		this.readEntry = readEntry;
	}
	/**
	 * @return the readOut
	 */
	public double getReadOut() {
		return readOut;
	}
	/**
	 * @param readOut the readOut to set
	 */
	public void setReadOut(double readOut) {
		this.readOut = readOut;
	}
	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
}

package br.com.water4solution.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.sun.istack.NotNull;

@Entity
public class Arduino implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4581772736648460881L;
	@Id @GeneratedValue @NotNull
	private int id;
	@NotNull
	private String macAdress;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the macAdress
	 */
	public String getMacAdress() {
		return macAdress;
	}
	/**
	 * @param macAdress the macAdress to set
	 */
	public void setMacAdress(String macAdress) {
		this.macAdress = macAdress;
	}


	
}

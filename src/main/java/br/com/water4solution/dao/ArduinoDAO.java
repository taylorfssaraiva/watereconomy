package br.com.water4solution.dao;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.water4solution.model.Arduino;

public class ArduinoDAO {
	private Session session;
	private Logger log;
	
	public ArduinoDAO() {
		log = Logger.getLogger(ArduinoDAO.class);
	}

	public Arduino select(Arduino arduino) {
		log.debug("Select - START");
		session = ConnectionFactory.getSession();
		Arduino arduinoTemp = (Arduino) session.createCriteria(Arduino.class).
				add(Restrictions.eq("macAdress", arduino.getMacAdress())).
				uniqueResult();
		log.debug("Select - END");
		session.close();
		return arduinoTemp;
	}

	public void update(Arduino arduino) {
		// TODO Auto-generated method stub
		
	}
}

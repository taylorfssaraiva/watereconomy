package br.com.water4solution.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import br.com.water4solution.model.Arduino;
import br.com.water4solution.model.RecordWater;

public class WaterRecordDAO {
	private Session session;
	private Logger log;

	public WaterRecordDAO() {
		log = Logger.getLogger(ArduinoDAO.class);
	}

	public List<RecordWater> select(Arduino ardId) {
		log.debug("Select - START");
		session = ConnectionFactory.getSession();
		@SuppressWarnings("unchecked")
		List<RecordWater> recordList = session.createCriteria(RecordWater.class)
				.add(Restrictions.eq("arduino", ardId)).list();
		session.close();
		log.debug("Select - END");
		return recordList;
	}

	public void update(RecordWater recWater) {
		// TODO Auto-generated method stub

	}

	public void insert(RecordWater recWater) {
		RecordWater recordWater = recWater;
		session = ConnectionFactory.getSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.save(recordWater);
			tx.commit();
		} catch (Exception e) {
			log.error("Insert RecordWater - ", e);
		}finally{
			session.close();
		}
	}

	public RecordWater getRecordById(RecordWater recordWater) {
		log.debug("getRecordById - START");
		session = ConnectionFactory.getSession();
		RecordWater recordList = (RecordWater) session.get(RecordWater.class, recordWater.getRecordId());
		session.close();
		log.debug("getRecordById - END");
		return recordList;
	}

}
